#include "comunicacion.h"




/* Funci�n para crear el socket y enlazarlo a un puerto
 */

CodigoMensaje CrearSocket(SOCKET *sd_dg,int puerto)
{
    struct sockaddr_in saddr_loc;
    int iRc;
    CodigoMensaje resultado;


// Inicializar el Socket; solo necesario para Windows
#ifdef _WIN32
    WSADATA wsadata;
    WORD wVer;

    wVer= MAKEWORD(1,1);
    iRc = WSAStartup(wVer,&wsadata);
    if (iRc != 0) {
        resultado = RecogerError("Error occurred on WSAStartup()");
	return resultado;
    }
        
    if (LOBYTE (wsadata.wVersion) != 1 ||
        HIBYTE (wsadata.wVersion) != 1 ) {
        resultado = RecogerError("WINSOCK.DLL wrong version");
	return resultado;
    }
#endif

// Crea el socket datagram
    *sd_dg = socket(AF_INET,SOCK_DGRAM,IPPROTO_IP);
    if (*sd_dg < 0) 
    {
        resultado = RecogerError("Error al crear socket");
	return resultado;
    }

// Datos del host donde se encuentra el cliente
    saddr_loc.sin_family=AF_INET;
    saddr_loc.sin_port=htons(puerto);
    saddr_loc.sin_addr.s_addr=htonl(INADDR_ANY);
    
// Puede hacerse el bind o conenct, pero no son necesarios.
// Habra que estudiarlo.
//     iRc=bind(*sd_dg,(struct sockaddr *) &saddr_loc,sizeof(saddr_loc));
//     if (iRc < 0) {
//         resultado = RecogerError("Error on bind()");
// 	return resultado;
//     } 
    

// Todo ha ido bien.
    resultado.mensaje = "OK";
    resultado.codigo = 1;  
    return resultado;
}

////////////////////////////////////////
/* Funci�n para Leer mensaje del socket
 */
CodigoMensaje LeeMensaje(SOCKET sd_dg,instruccion *Instruccion, struct sockaddr_in *from)
{
    static struct sockaddr_in saddr_remote;
    int iRc;
    int iAddrLen;
    CodigoMensaje resultado;


    iAddrLen = sizeof(saddr_remote);
    iRc = recvfrom(sd_dg, (char FAR *) Instruccion,sizeof(instruccion),0,(struct sockaddr FAR *) &saddr_remote, & SOCKLEN_T iAddrLen); 
    if (iRc < 0)
    {
        resultado = RecogerError("Error en recvfrom");
	return resultado;
    } 
    memcpy(from,&saddr_remote,sizeof(struct sockaddr_in));
		
    resultado.mensaje = "OK";

    string temp;
    if(Instruccion->Codigo == 0){
  	temp = "Error; ";
    }
    else if(Instruccion->Codigo ==1)
    {
  	temp = "OK; ";
    } 
    temp.append(Instruccion->Instruc);
    temp.append("\n");

    resultado.codigo = 1;
    resultado.mensaje = temp;
    return resultado;
}

////////////////////////////////////////
/* Funcion para enviar Socket.
 */

CodigoMensaje EnviaMensaje(SOCKET iSocket,int port,char *host, instruccion *Instruccion)
{
    struct sockaddr_in siSockAddr;
    int iRc;
    CodigoMensaje resultado;
    
    if (iSocket != INVALID_SOCKET)
    {
	memset((char *)&siSockAddr,0,sizeof(siSockAddr));
	siSockAddr.sin_family = AF_INET;
	siSockAddr.sin_port = htons(port);
	siSockAddr.sin_addr.s_addr = inet_addr(host);
	iRc = sendto(iSocket,(char const FAR *)Instruccion,sizeof(instruccion),0,(struct sockaddr FAR *)(&siSockAddr),sizeof(siSockAddr));
	if (iRc == -1) 
	{
	    resultado = RecogerError("Error en sendto()");
	    return resultado;
	}
	resultado.mensaje = "OK";
	resultado.codigo = 1;  
	return resultado;    
    }
    else {
	resultado = RecogerError("Identificador socket invalido en sendto()");
	return resultado;
    }
}



////////////////////////////////////////

/* Funci�n para cerrar el Socket.
 */

void CierraSocket(SOCKET *sd_dg)
{
#ifdef _WIN32
    closesocket(*sd_dg);  
    WSACleanup();
#else
    close(*sd_dg);
#endif
}

////////////////////////////////////////
CodigoMensaje Cliente(SOCKET fd_sock, instruccion* orden, instruccion* respuesta)
{
    fd_set fds;
    struct timeval tv;
    int n,retransmition;
    CodigoMensaje resultado;
    struct sockaddr_in *from;

// Inicializaci�n de variables    
    
    from = new struct sockaddr_in;
    retransmition = -1;

//    printf("************************\n");

// retransmition controla el numero de veces que se ha reenviado un socket 
// porque no se ha recibido respuesta. 

 retransmit:
    retransmition++;

    EnviaMensaje(fd_sock,TARGETPORT,MYHOST,orden);
    
// Tiempo a esperar antes de remandar un socket. A esto se le llama 
// retrasmision lineal. Ver cap 20 de "UNIX network programming I" (W. Richard
// Stevens) para un calculo de retrasmision mejor (y mas complicado).
// Yo creo que con la retrasmision lineal no habra problemas ya que es de
// esperar que la tasa de sockets perdidos sea muy peque�a al trabajar dentro
// de la misma subred.
    FD_ZERO(&fds);
    FD_SET(fd_sock, &fds);
    tv.tv_sec=2;
    tv.tv_usec=0;
    n = select(fd_sock+1,&fds,NULL,NULL,&tv);
 
// Tal vez, hay que definir distintos errores a devolver. Pero eso queda 
// para luego.   
    if(n==0) // Se acabo el tiempo
    {
	printf("RETRANSMITION : %i\n", retransmition);
	if(retransmition==MAXRETRANSMIT)
	{
	    resultado = RecogerError("NUMERO MAXIMO DE RETANSMISIONES");
	    return resultado;
	}
	goto retransmit;
    }
    else if(n<0) // Error in select()
    {
	resultado = RecogerError("Error en select()");
	return resultado;
    }

    resultado = LeeMensaje(fd_sock, respuesta, from);
    return resultado;
}
////////////////////////////////////////

/* Funci�n para recoger los errores
 */
// Tal vez hay que cambiar, para que se pueda devolver distinto enteros
// que definan distintos errores.
CodigoMensaje RecogerError(const char *MensajeError) 
{
    CodigoMensaje resultado;
// El mensaje al stderr
#ifdef _WIN32
    fprintf(stderr,"%s :%d\n",MensajeError,errno);
#else
    perror(MensajeError);
#endif
    resultado.mensaje = MensajeError;
    resultado.codigo = 0;
    return resultado;
}

////////////////////////////////////////

/* Funcion para inicializar una instruccion con todos los campos a cero.
 */

void init_instruccion(instruccion* orden)
{
    orden->Handle = 0;
    orden->Codigo = 0;
    int i;
    for(i=0;i<10;i++)
    {
	orden->Param[i] = 0;
    }
    strcpy(orden->Comando,"");
    strcpy(orden->Instruc,"");
}
