/////////////////////////////////////////////////////////////////////////////
// Name:        control.cpp
// Author:      XX
// Created:     XX/XX/XX
// Copyright:   
/////////////////////////////////////////////////////////////////////////////

#ifdef __GNUG__
    #pragma implementation "control.h"
#endif

// For compilers that support precompilation
#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

// Include private headers
#include "control.h"
#include "control_wdr.h"
#include "TerminalDlg.h"
#include "AlineamientoDlg.h"

// Include icon header
#if defined(__WXGTK__) || defined(__WXMOTIF__)
    #include "mondrian.xpm"
#endif

int  minimun(int a, int b)
{
    if(a > b)return b;
     else return a;
}


//----------------------------------------
// DEFINITIONS
//----------------------------------------
#define APP_NAME wxT("DifracControl")
#define APP_VENDOR wxT("EHU-UPV")


// WDR: class implementations

//------------------------------------------------------------------------------
// MyFrame
//------------------------------------------------------------------------------

// WDR: event table for MyFrame

BEGIN_EVENT_TABLE(MyFrame,wxFrame)
    EVT_MENU(ID_ABOUT, MyFrame::OnAbout)
    EVT_MENU(ID_QUIT, MyFrame::OnQuit)
    EVT_MENU(ID_ABRIR, MyFrame::OnMenuArchivoAbrir)
    EVT_MENU(ID_TERMINAL, MyFrame::OnMenuEjecutarTerminal)
    EVT_MENU(ID_ALINEAMIENTO,MyFrame::OnMenuEjecutarAlineamiento)
    EVT_CLOSE(MyFrame::OnCloseWindow)
END_EVENT_TABLE()

MyFrame::MyFrame( wxWindow *parent, wxWindowID id, const wxString &title,
    const wxPoint &position, const wxSize& size, long style ) :
    wxFrame( parent, id, title, position, size, style )
{
// From wxGuide section 2.1
// Sets the initial size of the application.
    SetSize (DetermineFrameSize ());

    CreateMyMenuBar();
    CreateMyToolBar();
    CreateStatusBar(1);
    SetStatusText( wxT("Control difractůmetro") );
    
    SetIcon(wxICON(mondrian));
    
     // insert main window here

}

MyFrame::~MyFrame()
{
}

void MyFrame::CreateMyMenuBar()
{
    SetMenuBar( CreateMyMenuBarFunc() );
}

void MyFrame::CreateMyToolBar()
{
    wxToolBar *tb = CreateToolBar( wxTB_HORIZONTAL|wxNO_BORDER );
    
    MyToolBarFunc( tb );
}

// WDR: handler implementations for MyFrame

void MyFrame::OnAbout( wxCommandEvent &event )
{
    wxDialog dialog( this, -1, wxT("About control"), wxDefaultPosition );
    AboutDialogFunc( &dialog, TRUE );
    dialog.CentreOnParent();
    dialog.ShowModal();
}

void MyFrame::OnQuit( wxCommandEvent &event )
{
     Close( TRUE );
}

void MyFrame::OnCloseWindow( wxCloseEvent &event )
{
    // if ! saved changes -> return
    
    Destroy();
}

// From wxGuide section 2.1
// Set the initial size of the frame
wxRect MyFrame::DetermineFrameSize () {
    wxSize display = wxGetDisplaySize();
    wxRect normal;
    if (display.x <= 640) {
        normal.x = 0;
        normal.width = display.x;
    }else if (display.x <= 800) {
        normal.x = display.x / 12;
        normal.width = display.x - normal.x - 10;
    }else if (display.x <= 1024) {
        normal.x = display.x / 10;
        normal.width = minimun (display.x - normal.x - 20, 840); // about A4
    }else{
        normal.x = 1024 / 10; // same as x=1024
        normal.width = 840; // about A4
    }
    if (display.y <= 640) {
        normal.y = 0;
        normal.height = display.y;
    }else if (display.y <= 800) {
        normal.y = 0;
        normal.height = display.y - normal.y - 30;
    }else if (display.y <= 1024) {
        normal.y = 10;
        normal.height = display.y - normal.y - 30;
    }else{
        normal.y = 20;
        normal.height = 1200; // about A4
    }     return normal;
}


/* Process Menu Archivo | Abrir */
void MyFrame::OnMenuArchivoAbrir( wxCommandEvent &event )
{
   wxDialog dialog( this, ID_OPEN_DLG, wxT("Test foreign control"), wxDefaultPosition );
   wxFileDialog *dlg =  new wxFileDialog(&dialog, wxT("Abrir Archvio"),wxT(""),wxT(""),                                        "Todos(*.*)|*.*",wxOPEN, wxDefaultPosition);
//    wxFileDialog *dlg = new wxFileDialog(this, "Abrir Archivo", "", "", 
//                                        "Todos(*.*)|*.*",wxOPEN, wxDefaultPosition);   
                                       
    if (dlg->ShowModal() == wxID_OK)
    {
        SetStatusText(dlg->GetFilename(),0);
    }
     dlg->Destroy();
}   

/* Process Menu Ejecutar | Terminal */
void MyFrame::OnMenuEjecutarTerminal( wxCommandEvent &event )
{
    TerminalDlg dialog(this, -1, wxT("Terminal") );
    dialog.CentreOnParent();
    dialog.ShowModal();
}


/* Procesar Menu | Ejecutar | Alineamiento */
void MyFrame::OnMenuEjecutarAlineamiento(wxCommandEvent &event)
{
    AlineamientoDlg dialog(this, -1, wxT("Alineamiento") );
    dialog.CentreOnParent();
    dialog.ShowModal();
}

//------------------------------------------------------------------------------
// MyApp
//------------------------------------------------------------------------------

IMPLEMENT_APP(MyApp)

MyApp::MyApp()
{
}

bool MyApp::OnInit()
{
    SetAppName (APP_NAME);
    SetVendorName (APP_VENDOR);

// create application frame
    MyFrame *frame = new MyFrame( NULL, -1, wxT("control"), wxPoint(20,20), wxSize(500,340) );
    frame->Show( TRUE );
    
    return TRUE;
}

int MyApp::OnExit()
{
    return 0;
}



