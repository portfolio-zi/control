/////////////////////////////////////////////////////////////////////////////
// Name:        AlineamientoDlg.cpp
// Author:      XX
// Created:     XX/XX/XX
// Copyright:   XX
/////////////////////////////////////////////////////////////////////////////

#ifdef __GNUG__
    #pragma implementation "TerminalDlg.cpp"
#endif

// For compilers that support precompilation, includes "wx/wx.h".
#include <wx/wxprec.h>

#ifdef __BORLANDC__
    #pragma hdrstop
#endif

#include "AlineamientoDlg.h"
//#include "global.h"
#include "instruccion.h"
#include <wx/valgen.h>
#include "definit.h"

#ifdef _WIN32

#endif

// WDR: class implementations



//----------------------------------------------------------------------------
// TerminalDlg
//----------------------------------------------------------------------------

// WDR: event table for TerminalDlg

BEGIN_EVENT_TABLE(AlineamientoDlg,wxDialog)
    EVT_BUTTON(ID_ALINEAMIENTO_ALINEAR_BUTTON,AlineamientoDlg::Medir)
END_EVENT_TABLE()

AlineamientoDlg::AlineamientoDlg(wxWindow *parent, wxWindowID id, const wxString &title,
    const wxPoint &position, const wxSize& size, long style ) :
    wxDialog( parent, id, title, position, size, style )
{
    AlineamientoDialogFunc( this, TRUE ); 

// Apuntadores al los controles de texto del cuadro de dialogo.    
     anchopasostext = GetAnchoTextctrl();
     tiempotext = GetTiempoTextctrl();
     numeropasostext = GetNumeroTextctrl();


     anchostring = wxT("");
     anchopasostext->SetValidator(wxGenericValidator(&anchostring));
     tiempostring = wxT("");
     tiempotext->SetValidator(wxGenericValidator(&tiempostring));
     anchostring = wxT("");
     anchopasostext->SetValidator(wxGenericValidator(&anchostring));

}


AlineamientoDlg::~AlineamientoDlg()
{
}

void AlineamientoDlg::Medir()
{
// // Por cada caja de texto tenemos un "validator"
//     wxGenericValidator* valinstruccion = (wxGenericValidator*) instrucciontext->GetValidator();
//     valinstruccion->TransferFromWindow();


//     wxGenericValidator* valcomando = (wxGenericValidator*) comandotext->GetValidator();
//     valcomando->TransferFromWindow();


//     wxGenericValidator* valparametros0 = (wxGenericValidator*) parametros0text->GetValidator();
//     valparametros0->TransferFromWindow();
//     wxGenericValidator* valparametros1 = (wxGenericValidator*) parametros1text->GetValidator();
//     valparametros1->TransferFromWindow();


//     wxGenericValidator* valcodigo = (wxGenericValidator*) codigotext->GetValidator();
//     valcodigo->TransferFromWindow();


//     wxGenericValidator* valrespuesta = (wxGenericValidator*) respuestatext->GetValidator();
//     valrespuesta->TransferFromWindow();
//      wxGenericValidator* valhandle = (wxGenericValidator*) handletext->GetValidator();
//      valhandle->TransferFromWindow();



//     instruccion* sck_respuesta = new instruccion;
//     instruccion* orden = new instruccion;
//     from = new struct sockaddr_in;

// // Rellenar orden, que es la instrucción que se manda

// // Copia handle al miembro de la instruccion que se manda
//      long int Longint;
//      handlestring.ToLong(&Longint);
//      orden->Handle = (unsigned short int) Longint;
// // Copia orden al miembro de la instruccion que se manda
//      codigostring.ToLong(&Longint);
//      orden->Codigo = (unsigned short int) Longint;
// // Copia parametros al miembro de la instruccion que se manda
//      orden->Param[0] = atof(parametros0string.c_str());
//      orden->Param[1] = atof(parametros1string.c_str());
// // Copia comando al miembro de la instruccion que se manda
//      strcpy(orden->Comando,comandostring.c_str());
// // Instruccion
//      strcpy(orden->Instruc,"");


// // Enviar la orden
// //      com_respuesta = EnviaMensaje(socket_dg, TARGETPORT,MYHOST, orden);
// //      if (com_respuesta.codigo < 0) fprintf(stderr,"Error\n");
// //  Recibir la respuesta a la orden
// //      com_respuesta = LeeMensaje(socket_dg, sck_respuesta,from );
// //      if (com_respuesta.codigo == 0) fprintf(stderr,"Error respuesta\n");
	
//      com_respuesta = Cliente(socket_dg, orden, sck_respuesta);
	
//      if(com_respuesta.codigo ==0)
//      {
// 	 perror(com_respuesta.mensaje.c_str());
//      }

// // Pasar la información del socket de repuesta al cuadro de dialogo. 
// // Solo es necesario mantener la linea referida a respuestatext, las otras 
// // estan para comprobar que se debuelve en la respuesta.
//      handletext->SetValue(wxString::Format("%d",sck_respuesta->Handle));
//      codigotext->SetValue(wxString::Format("%d",sck_respuesta->Codigo));
//      comandotext->SetValue(sck_respuesta->Comando);
//      instrucciontext->SetValue(sck_respuesta->Instruc);
//      parametros0text->SetValue(wxString::Format("%f",sck_respuesta->Param[0]));
//      parametros1text->SetValue(wxString::Format("%f",sck_respuesta->Param[1]));
//      respuestatext->AppendText(com_respuesta.mensaje.c_str());
       
//      delete from;
//      delete orden, sck_respuesta;

// //      Vaciar las cajas de texto
// //       instruccionstring = wxT("");
// //       valinstruccion->TransferToWindow();
// //       comandostring = wxT("");
// //       valcomando->TransferToWindow();
    // //       parametrosstring = wxT("");
// //       valparametros->TransferToWindow();
// //       codigostring = wxT("");
// //       valcodigo->TransferToWindow();
}

// WDR: handler implementations for TerminalDlg




