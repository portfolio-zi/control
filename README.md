control
-------

Low level control for a X-ray powder diffractometer. 
The original program was developed at TEKNIKER, where the diffractometer 
was built. 

I added some functionallity while developing the higher control program pycontrol
during my PhD. thesis work.