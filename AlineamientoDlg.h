/////////////////////////////////////////////////////////////////////////////
// Name:        AlineamientoDlg.h
// Author:      XX
// Created:     XX/XX/XX
// Copyright:   XX
/////////////////////////////////////////////////////////////////////////////

#ifndef __ALINEAMIENTODLG_H__
#define __ALINEAMIENTODLG_H__

#ifdef __GNUG__
    #pragma interface "TerminalDlg.cpp"
#endif

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "control_wdr.h"
#include "definit.h"

#include "instruccion.h"
#include "comunicacion.h"

extern wxString anchostring;
extern wxString tiempostring;
extern wxString numerostring;

// WDR: class declarations

//----------------------------------------------------------------------------
// AlineamientoDlg
//----------------------------------------------------------------------------

class AlineamientoDlg: public wxDialog
{
public:
    // constructors and destructors
    AlineamientoDlg( wxWindow *parent, wxWindowID id, const wxString &title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_DIALOG_STYLE );
    ~AlineamientoDlg();
    void Medir();
//    CodigoMensaje com_respuesta;
//    SOCKET socket_dg;
//    struct sockaddr_in*  from;

    // WDR: method declarations for TerminalDlg

private:
    // WDR: member variable declarations for TerminalDlg

    wxTextCtrl* GetAnchoTextctrl()  { return (wxTextCtrl*) FindWindow( ID_ALINEAMIENTO_ANCHOPASOS_TEXTCTRL ); }
    wxTextCtrl* GetTiempoTextctrl()  { return (wxTextCtrl*) FindWindow( ID_ALINEAMIENTO_TIEMPO_TEXTCTRL ); }
    wxTextCtrl* GetNumeroTextctrl()  { return (wxTextCtrl*) FindWindow( ID_ALINEAMIENTO_NUMEROPASOS_TEXTCTRL ); }

    wxTextCtrl* anchopasostext;
    wxTextCtrl* tiempotext;
    wxTextCtrl* numeropasostext;

private:
    // WDR: handler declarations for TerminalDlg

private:
    DECLARE_EVENT_TABLE()
};


#endif
