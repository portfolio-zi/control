/////////////////////////////////////////////////////////////////////////////
// Name:        control.h
// Author:      XX
// Created:     XX/XX/XX
// Copyright:   
/////////////////////////////////////////////////////////////////////////////

#ifndef __control_H__
#define __control_H__

#ifdef __GNUG__
    #pragma interface "control.h"
#endif

//include wxWindows' headers

#ifndef WX_PRECOMP
    #include "wx/wx.h"
#endif



#include <wx/config.h>   // configuration support
#include <wx/gdicmn.h>   // manipulation of rectangles


// min function 
int minimun(int , int );

#include "global.h"
//----------------------------------------------------------------------------
//   constants
//----------------------------------------------------------------------------



// WDR: class declarations

//----------------------------------------------------------------------------
// MyFrame
//----------------------------------------------------------------------------

class MyFrame: public wxFrame
{
public:
    // constructors and destructors
    MyFrame( wxWindow *parent, wxWindowID id, const wxString &title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_FRAME_STYLE );
    ~MyFrame();
// From wxGuide section 2.1
// determine default frame position/size
//    wxRect DetermineFrameSize (wxConfig* config);
   wxRect DetermineFrameSize ();



    
private:
    // WDR: method declarations for MyFrame
    void CreateMyMenuBar();
    void CreateMyToolBar();
    
private:
    // WDR: member variable declarations for MyFrame

    
    
private:
    // WDR: handler declarations for MyFrame
    void OnAbout( wxCommandEvent &event );
    void OnQuit( wxCommandEvent &event );
    void OnCloseWindow( wxCloseEvent &event );
    void OnMenuArchivoAbrir( wxCommandEvent &event );
    void OnMenuEjecutarTerminal(wxCommandEvent &event);
    void OnMenuEjecutarAlineamiento(wxCommandEvent &event);
private:
    DECLARE_EVENT_TABLE()
};




//----------------------------------------------------------------------------
// MyApp
//----------------------------------------------------------------------------

class MyApp: public wxApp
{
public:
    MyApp();
    
    virtual bool OnInit();
    virtual int OnExit();
};

DECLARE_APP(MyApp)

#endif
