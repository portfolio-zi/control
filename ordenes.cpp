#include "ordenes.h"


/*  En este archivo se defines las ordenes que hay mandar al difractometro
como funciones, para poder ejecutar una secuencia de ellas.
*/

// Indica que esa posici�n de la mesa es redefinida como la posici�n cero 
//de la mesa th
CodigoMensaje Cero_th(SOCKET* socket_dg, UINT handle, instruccion* sck_respuesta)
{
    CodigoMensaje resultado;
    instruccion orden;
   
    init_instruccion(&orden);
    orden.Handle = handle;
    orden.Codigo = 1;

    resultado = Cliente(socket_dg, TARGETPORT, MYHOST, orden, sck_respuesta, from);
    return resultado;
}

// Indica que esa posici�n de la mesa es redefinida como la posici�n cero 
//de la mesa tth
CodigoMensaje Cero_th(SOCKET* socket_dg, UINT handle, instruccion* sck_respuesta)
{
    CodigoMensaje resultado;
    instruccion orden;
   
    init_instruccion(&orden);
    orden.Handle = handle;
    orden.Codigo = 2;

    resultado = Cliente(socket_dg, TARGETPORT, MYHOST, orden, sck_respuesta, from);
    return resultado;
}


// Abrir el shuter
Codigo Mensaje AbrirShutter(SOCKET* socket_dg, UINT handle, instruccion* sck_respuesta)
{
    CodigoMensaje resultado;
    instruccion orden;

    init_instruccion(&orden);
    orden.Handle = handle;
    orden.Codigo = 13;
    
    resultado = Cliente(socket_dg, TARGETPORT, MYHOST, orden, sck_respuesta, from);
}

// Cerrar el shuter
CodigoMensaje CerrarShutter(SOCKET* socket_dg, UINT handle, instruccion* sck_respuesta)
{
    CodigoMensaje resultado;
    instruccion orden;

    init_instruccion(&orden);
    orden.Handle = handle;
    orden.Codigo = 12;
}
