#ifndef __COMUNICACION_H
#define __COMUNICACION_H


#ifdef _WIN32
#include <windows.h>
#include <winsock.h>
#include <stdlib.h>
#include <stdio.h>
#else
#include <sys/socket.h>
#include <arpa/inet.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#endif 

// DEFINICIONES PARA COMPATIBILIDAD WINDOWS/LINUX
#ifndef _WIN32 // Para Unix
#define INVALID_SOCKET -1
#define SOCKET_ERROR -1
#define FAR 
#define SOCKLEN_T (socklen_t)

#else // Para Windows
#define SOCKLEN_T 
#define errno WSAGetLastError()
#endif
// FIN DEFINICIONES PARA COMPATIBILIDAD WINDOWS/LINUX


#include <string>
#include "definit.h"
#include "instruccion.h"

/* Las funciones de comunicaci�n devuelven esta estructura. 
El codigo es un entero que vale 1 si el funcionamiento es 
satisfactorio y 0 si hay alg�n error. La cadena mensaje
contendra la descripci�n el error en caso de haberlo.
*/
struct CodigoMensaje {
    int codigo;
    string mensaje;
};

// El numero maximos de retrasmisiones antes de parar.
#define MAXRETRANSMIT 10



////////////////////////////////////////

/* Prototipo de funciones
 */


CodigoMensaje CrearSocket(SOCKET *sd_dg,int puerto);
CodigoMensaje LeeMensaje(SOCKET sd_dg,instruccion *Instruccion, struct sockaddr_in *from);
CodigoMensaje EnviaMensaje(SOCKET iSocket,int port,char *host, instruccion *Instruccion);
void CierraSocket(SOCKET *sd_dg);
CodigoMensaje Cliente(SOCKET fd_sock, instruccion* orden, instruccion* respuesta);
CodigoMensaje RecogerError(const char *MensajeError);
void init_instruccion(instruccion* orden);
#endif
