#include "alineamiento.h"

int GetAlignmentMaximun(double angle[], double intensity[], 
			int pointnumber, double center)
{
    gsl_vector *anglevector = gsl_vector_alloc(pointnumber);
    gsl_vector *intensityvector = gsl_vector_alloc(pointnumber);

// Copia los array de los datos obtenidos del difractometro a unos 
// vectores de la libreria gsl.
    int i;
    for(i=0;i<pointnumber;i++)
    {
	gsl_vector_set(anglevector,i,angle[i]);
	gsl_vector_set(intensityvector,i,intensity[i]);
    }

// Buscamos la intensitdad maxima y el indice del vector correspondiente
// al maximo

    double intensitymax = gsl_vector_max(intensityvector);
    int intensitymaxindex = gsl_vector_max_index(intensityvector);

// El cspline lo haremos en torno al maximo. Los valores que acoten la
// región seran el CSPLINEINTENSITY % 

    double intensityspline = (intensitymax * CSPLINEINTENSITY) / 100;
    
    int index1; // El indice menor que da intensityspline
    for(index1=0;index1<=pointnumber;index1++)
    {
	if(gsl_vector_get(intensityvector,index1)>intensityspline) 
	    break;
    }

    int index2; // El indice mayor que da intensityspline
    for(index2=pointnumber;index2>=0;index2--)
    {
	if(gsl_vector_get(intensityvector,index2)>intensityspline) 
	    break;
    }

    int splinepointnumber = index2-index1+1; //El numero de puntos en 
                                             //en el cspline

    double splineangle[splinepointnumber]; // Angulos para el spline
    double splineintensity[splinepointnumber]; // Intensidades para spline

    
    for(i=0;i<splinepointnumber;i++)
    {
	splineangle[i] = gsl_vector_get(anglevector,index1+i);
	splineintensity[i] = gsl_vector_get(intensityvector,index1+i);
    }

// Interpolar
    gsl_interp_accel *acc; // acelearador para el spline
    gsl_spline *spline;    // el spline
    
    acc = gsl_interp_accel_alloc();
    spline = gsl_spline_alloc(gsl_interp_cspline,splinepointnumber);
    gsl_spline_init(spline,splineangle,splineintensity,splinepointnumber);

// Maximización.

// agnlemin es el angulo minimo donde empieza el spline que vamos a maximizar.
// agnlemax es el angulo maximo donde termina el spline que vamos a maximizar.
// anglemed es el puhto medio, desde donde vamos a empezar a maximizar.
    double anglemin = gsl_vector_get(anglevector,index1);
    double anglemax = gsl_vector_get(anglevector,index2);
    double anglemed = (anglemin+anglemax)/2;

    const gsl_min_fminimizer_type *P;
    gsl_min_fminimizer *s;

    gsl_function F; // Funcion a minimizar
    minimization_params params = {spline,acc };
    F.function = &function;
    F.params = &params;
 
    P = gsl_min_fminimizer_brent; // algoritmo de minimización
    s = gsl_min_fminimizer_alloc(P);

    gsl_min_fminimizer_set(s,&F,anglemed,anglemin,anglemax);


    int iter = 0; // Iteraciones para minimizar 
    int iter_max=100; // Numero maximo de iteraciones
    int status;   // entero de control
    do
    {
	iter++;
	status = gsl_min_fminimizer_iterate(s);
	
	anglemed = gsl_min_fminimizer_minimum(s);
	anglemin = gsl_min_fminimizer_x_lower(s);
	anglemax = gsl_min_fminimizer_x_upper(s);
	
	status = gsl_min_test_interval(anglemin,anglemax,0.0001,0.0);
	    
    }
    while(status ==GSL_CONTINUE && iter<iter_max);

    center = anglemed;

// Liberar memoria
    gsl_min_fminimizer_free(s);
    gsl_spline_free(spline);
    gsl_interp_accel_free(acc);
    gsl_vector_free(anglevector);
    gsl_vector_free(intensityvector);

    return 0;
}

