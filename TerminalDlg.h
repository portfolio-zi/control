/////////////////////////////////////////////////////////////////////////////
// Name:        TerminalDlg.h
// Author:      XX
// Created:     XX/XX/XX
// Copyright:   XX
/////////////////////////////////////////////////////////////////////////////

#ifndef __TERMINALDLG_H__
#define __TERMINALDLG_H__

#ifdef __GNUG__
    #pragma interface "TerminalDlg.cpp"
#endif

#ifndef WX_PRECOMP
    #include <wx/wx.h>
#endif

#include "control_wdr.h"
#include "definit.h"

#include "instruccion.h"
#include "comunicacion.h"


extern wxString handlestring;
extern wxString instruccionstring;
extern wxString comandostring;
extern wxString parametros0string;
extern wxString parametros1string;
extern wxString codigostring;
extern wxString respuestastring;

// WDR: class declarations

//----------------------------------------------------------------------------
// TerminalDlg
//----------------------------------------------------------------------------

class TerminalDlg: public wxDialog
{
public:
    // constructors and destructors
    TerminalDlg( wxWindow *parent, wxWindowID id, const wxString &title,
        const wxPoint& pos = wxDefaultPosition,
        const wxSize& size = wxDefaultSize,
        long style = wxDEFAULT_DIALOG_STYLE );
    ~TerminalDlg();
    void Enviar();
    CodigoMensaje com_respuesta;
    SOCKET socket_dg;
    struct sockaddr_in*  from;

    // WDR: method declarations for TerminalDlg

private:
    // WDR: member variable declarations for TerminalDlg
    // Getters para los campos
    wxTextCtrl* GetInstruccionTextctrl()  { return (wxTextCtrl*) FindWindow( ID_INSTRUCCION_TEXTCTRL ); }
    wxTextCtrl* GetCodigoTextctrl()  { return (wxTextCtrl*) FindWindow( ID_CODIGO_TEXTCTRL ); }
    wxTextCtrl* GetParametros0Textctrl()  { return (wxTextCtrl*) FindWindow( ID_PARAMETROS0_TEXTCTRL ); }
    wxTextCtrl* GetParametros1Textctrl()  { return (wxTextCtrl*) FindWindow( ID_PARAMETROS1_TEXTCTRL ); }
    wxTextCtrl* GetComandoTextctrl()  { return (wxTextCtrl*) FindWindow( ID_COMANDO_TEXTCTRL ); }
    wxTextCtrl* GetRespuestaTextctrl()  { return (wxTextCtrl*) FindWindow( ID_RESPUESTA_TEXTCTRL ); }
    wxTextCtrl* GetHandleTextctrl()  { return (wxTextCtrl*) FindWindow( ID_HANDLE_TEXTCTRL ); }

  
    wxTextCtrl* instrucciontext; 
    wxTextCtrl* comandotext;
    wxTextCtrl* parametros0text;
    wxTextCtrl* parametros1text;
    wxTextCtrl* codigotext;
    wxTextCtrl* respuestatext;
    wxTextCtrl* handletext;

private:
    // WDR: handler declarations for TerminalDlg

private:
    DECLARE_EVENT_TABLE()
};


#endif
