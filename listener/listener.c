/*
** listener.c -- a datagram sockets "server" demo
*/

#ifdef _WIN32
#include <stdio.h>
#include <winsock.h>
#include <stdlib.h>
#define MYPORT 1020    // the port users will be connecting to
#define SOCKLEN_T
#define MYHOST "158.227.49.160"
#else
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#define FAR 
#define MYPORT 4950    // the port users will be connecting to#define FAR 
#define MYHOST "158.227.47.189"
typedef int SOCKET;
#define SOCKLEN_T (socklen_t *)
#endif

#include "instruccion.h"


int main(void)
{
    int sockfd, retransmition;
    struct sockaddr_in my_addr;    // my address information
    struct sockaddr_in their_addr; // connector's address information
    int addr_len, numbytes;
    instruccion orden, orden_anterior;
    int k;


#ifdef _WIN32
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(1,1), &wsaData) != 0)
    {
	fprintf(stderr,"WSASTARRuP");
	exit(1);
    }
#endif
    

    if ((sockfd = socket(AF_INET, SOCK_DGRAM, 0)) == -1) {
        perror("socket");
        exit(1);
    }


    my_addr.sin_family = AF_INET;         // host byte order
    my_addr.sin_port = htons(MYPORT);     // short, network byte order
    my_addr.sin_addr.s_addr = htonl(INADDR_ANY); // automatically fill with my IP
    memset(&(my_addr.sin_zero), '\0', 8); // zero the rest of the struct

    if (bind(sockfd, (struct sockaddr *)&my_addr,
		sizeof(struct sockaddr)) == -1) {
        perror("bind");
        exit(1);
    }



    addr_len = SOCKLEN_T sizeof(struct sockaddr);
    
    for(;;){
	retransmition =0;
	retrasmit:
	retransmition++;
	
	if ((numbytes=recvfrom(sockfd,(char FAR *) &orden, sizeof(orden) , 0,
			       (struct sockaddr *)&their_addr, &addr_len)) == -1) {
	    perror("recvfrom");
	    exit(1);
    }

    if(orden.Handle !=0)
    {
	if(orden.Handle == orden_anterior.Handle)
	{
	    printf("RECEBIDO MISMO SOCKET\n");
	    k = ((float)rand()/RAND_MAX)*100;
	    printf("RAND = %i RETRANSMITION = %i\n",k,retransmition); 
	    if(k<50){
		printf("RETRASMISION: %i\n",retransmition);
		if ((numbytes=sendto(sockfd,(char FAR *) &orden_anterior, sizeof(orden) , 0,(struct sockaddr *)&their_addr, addr_len)) == -1) 
		{
		    perror("sendto");
		    exit(1);
		}
		printf("ENVIADO RESPUESTA ANTERIOR EN MODO RETRASMITIR\n");
		
		continue;
	    }
	    else
	    {
		printf("SOCKET RETRASMISION PERDIDO\n");
		goto retrasmit;
	    }
	}
    }

    printf("*********************************************\n");
    printf("got packet from %s\n",inet_ntoa(their_addr.sin_addr));
    printf("packet is %d bytes long\n",numbytes);
    printf("Handle: %u\n",orden.Handle);
    printf("Codigo: %i\n",orden.codigo);
    printf("Param[0]: %f\n",orden.Param[0]);
    printf("Comando: %s\n",orden.Comando);
    printf("Instruccion: %s\n",orden.Instruc);
    printf("********************************************\n");
    
    orden.codigo=1;
    strcpy(orden.Instruc,"OK\n");


    k = ((float)rand()/RAND_MAX)*100;
    printf("RAND = %i\n",k); 
    if(k<80)
    {
	if ((numbytes=sendto(sockfd,(char FAR *) &orden, sizeof(orden) , 0,(struct sockaddr *)&their_addr, addr_len)) == -1) 
	{
	    perror("sendto");
	    exit(1);
	}
	printf("SOCKET MANDADO SIN PROBLEMAS\n");
    }
    else
    {
	printf("SOCKET PERDIDO\n");
    }
 orden_anterior.Handle = orden.Handle;
 orden_anterior.codigo = orden.codigo;
 orden_anterior.Param[0] = orden.Param[0];
 strcpy(orden_anterior.Comando,orden.Comando);
 strcpy(orden_anterior.Instruc,orden.Instruc);
 retransmition =0;
}

#ifdef _WIN32
    closesocket(sockfd);
    WSACleanup();
#else
    close(sockfd);
#endif
	return 0;
}
