#include <gsl/gsl_sf_erf.h> // Statistical functions
#include <gsl/gsl_rng.h>    // Random number generator
#include <gsl/gsl_randist.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_min.h>
#include <gsl/gsl_errno.h>
#include <gsl/gsl_math.h>



struct my_f_params {gsl_spline *  s; gsl_interp_accel *a; };

double function(double x, void * p)
{
  struct my_f_params * params
     = (struct my_f_params *)p;

  gsl_interp_accel  *a = (params->a);
  gsl_spline  *s = (params->s);
      
    return -1.0*gsl_spline_eval(s,x,a);
}

main(void)
{
    const gsl_rng_type *T;
    gsl_rng *r;
    int n=500,m;
    double yi,intensitymax,intensitysearch;
    double sigma = 1.0;
    double xmin,xmax,step,x,xi,xstep;
    gsl_vector *intensity = gsl_vector_alloc(n+1);
    gsl_vector *xar = gsl_vector_alloc(n+1);
    int indexmax, index1, index2;
    
    gsl_rng_env_setup();
    T= gsl_rng_default;

    r = gsl_rng_alloc(T);
    
    xmin = -10;
    xmax = 10;
    step = (xmax-xmin)/n;


    x = xmin;
    size_t i;
     for (i = 0;i<n;i++,x += step)
     {
 	double u = gsl_sf_erf_Z(x)+0.005*gsl_ran_gaussian(r,sigma);
 	gsl_vector_set(intensity,i, u);
 	printf("%20.18f %20.18f\n",x,u);
 	gsl_vector_set(xar,i,x);
     }
    printf("\n\n");

// buscar los indices 
    intensitymax = gsl_vector_max(intensity);
    indexmax = gsl_vector_max_index(intensity);
    intensitysearch = (intensitymax *70)/100;
    
    for(index1=0;index1<=n;index1++)
    {
	if(gsl_vector_get(intensity,index1)>intensitysearch) break;
    }

    for(index2=n;index2>=0;index2--)
    {
	if(gsl_vector_get(intensity,index2)>intensitysearch) break;
    }
    
    m=n*100;
    xstep = (xmax-xmin)/m;
    int nspline = index2-index1;
    double xars[nspline], ars[nspline];
    
    int k;
    for(k=0;k<nspline;k++)
    {
 	    xars[k] = gsl_vector_get(xar,index1+k);
 	    ars[k]  = gsl_vector_get(intensity,index1+k);
    }

// interpolar
	gsl_interp_accel *acc
	    = gsl_interp_accel_alloc();
	gsl_spline *spline
	    = gsl_spline_alloc(gsl_interp_cspline,nspline);
	gsl_spline_init(spline,xars,ars,nspline);
	
	xmin= gsl_vector_get(xar,index1);
	xmax= gsl_vector_get(xar,index2);
	
	
	
 	for(xi=xmin;xi<=xmax;xi+=xstep)
 	{
 	    yi = gsl_spline_eval(spline,xi,acc);
 	    printf("%20.18f %20.18f\n",xi,yi);
 	}

// Maximization
	printf("\n\n");
	double a = xmin, b=xmax;
	double med = (a+b)/2;
	const gsl_min_fminimizer_type *P;
	gsl_min_fminimizer *s;
	
	struct my_f_params params = {spline,acc };
	gsl_function F;
	F.function = &function;
	F.params = &params;
	P = gsl_min_fminimizer_brent;
	s = gsl_min_fminimizer_alloc(P);

	gsl_min_fminimizer_set(s,&F,med,a,b);

	int iter = 0, iter_max=100, status;
 	do
 	{
	    iter++;
 	    status = gsl_min_fminimizer_iterate(s);

	    med = gsl_min_fminimizer_minimum(s);
	    a = gsl_min_fminimizer_x_lower(s);
	    b = gsl_min_fminimizer_x_upper(s);

	    status = gsl_min_test_interval(a,b,0.0001,0.0);
	    
 	}
	while(status ==GSL_CONTINUE && iter<iter_max);
	printf("%f %f\n",med,gsl_spline_eval(spline,med,acc));
	
	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);
	
    return 0;
}
