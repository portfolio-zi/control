 /*
** talker.c -- a datagram "client" demo
*/
#ifdef _WIN32
#include <stdio.h>
#include <winsock.h>
#include <stdlib.h>
#define MYHOST "158.227.49.160"
#define MYPORT 1020
#define SOCKLEN_T
#else
#include <stdio.h>
#include <stdlib.h>
//#include <unistd.h>
//#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <arpa/inet.h>
#define FAR 
#define MYHOST "158.227.47.189"
#define MYPORT 4950    // the port users will be connecting to
#define SOCKLEN_T (socklen_t)
typedef int SOCKET;
#endif

#include "instruccion.h"


int main(int argc, char *argv[])
{
    int sockfd;
    struct sockaddr_in their_addr; // connector's address information
    struct hostent *he;
    int numbytes;
    instruccion orden;
#ifdef _WIN32
    WSADATA wsaData;
    if (WSAStartup(MAKEWORD(1,1), &wsaData) != 0)
    {
	fprintf(stderr,"WSASTARRuP");
	exit(1);
    }
#endif

//    if (argc != 3) {
//        fprintf(stderr,"usage: talker hostname message\n");
//        exit(1);
//    }

//    if ((he=gethostbyname(argv[1])) == NULL) {  // get the host info
    //       perror("gethostbyname");
//        exit(1);
//    }

    if ((sockfd = socket(AF_INET, SOCK_DGRAM,0)) == -1) {
        perror("socket");
        exit(1);
    }


    their_addr.sin_family = AF_INET;     // host byte order
    their_addr.sin_port = htons(MYPORT); // short, network byte order
    their_addr.sin_addr.s_addr = inet_addr(MYHOST);
//    their_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    memset(&(their_addr.sin_zero), '\0', 8);  // zero the rest of the struct


//    if((bind(sockfd,(struct sockaddr FAR *) &their_addr,sizeof(their_addr)))==-1)
//    {
//    perror("socket");
//        exit(1);
//    }

    orden.codigo = 6;
    strcpy(orden.Comando,"mvr tth 10");

    if ((numbytes=sendto(sockfd,(char FAR *)&orden,(size_t) sizeof(instruccion), 0,(struct sockaddr FAR *)&their_addr, SOCKLEN_T sizeof(struct sockaddr))) == -1) {
        perror("sendto");
        exit(1);
    }

    printf("sent %d bytes to %s\n", numbytes, inet_ntoa(their_addr.sin_addr));

#ifdef _WIN32
    closesocket(sockfd);
    WSACleanup();
#else
    close(sockfd);
#endif

    return 0;
}
