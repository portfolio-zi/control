#include <gsl/gsl_sf_erf.h> // Statistical functions
#include <gsl/gsl_rng.h>    // Random number generator
#include <gsl/gsl_randist.h>
#include <gsl/gsl_spline.h>
#include <gsl/gsl_vector.h>

//gsl_sf_erf_Z(x) // gaussian

main(void)
{
    const gsl_rng_type *T;
    gsl_rng *r;
    int i,n=500,m;
    double yi,intensitymax,intensitysearch;
    double sigma = 1.0;
    double xmin,xmax,step,x,xi,xstep;
    gsl_vector *intensity = gsl_vector_alloc(n+1);
    double xar[n], ar[n];
    int indexmax, index1, index2;
    
    gsl_rng_env_setup();
    T= gsl_rng_default;

    r = gsl_rng_alloc(T);
    
    xmin = -10;
    xmax = 10;
    step = (xmax-xmin)/n;

    x = xmin;
    for (i = 0;i<=n;i++,x += step)
    {
	double u = gsl_sf_erf_Z(x)+0.005*gsl_ran_gaussian(r,sigma);
	gsl_vector_set(intensity,i, u);
	printf("%20.18f %20.18f\n",x,u);
	xar[i]=x;
    }
    printf("\n\n");

    intensitymax = gsl_vector_max(intensity);
    indexmax = gsl_vector_max_index(intensity);
    intensitysearch = (intensitymax *70)/100;
    
    for(index1=0;index1<=n;index1++)
    {
	if(gsl_vector_get(intensity,index1)>intensitysearch) break;
    }

    for(index2=n;index2>=0;index2--)
    {
	if(gsl_vector_get(intensity,index2)>intensitysearch) break;
    }
    
    printf("%f %f\n",intensitymax, intensitysearch);
    printf("%f %f\n",gsl_vector_get(intensity,index1),gsl_vector_get(intensity,index2));
    printf("%d %d %d\n",index1, indexmax, index2);
    m=n*100;
    xstep = (xmax-xmin)/m;
    {
	gsl_interp_accel *acc
	    = gsl_interp_accel_alloc();
	gsl_spline *spline
	    = gsl_spline_alloc(gsl_interp_cspline,n);
/*	gsl_spline_init(spline,xar,ar,n);*/
	gsl_spline_init(spline,xar,intensity,n);

	for(xi=xmin;xi<=xmax;xi+=xstep)
	{
	    yi = gsl_spline_eval(spline,xi,acc);
	    printf("%20.18f %20.18f\n",xi,yi);
	}
	gsl_spline_free(spline);
	gsl_interp_accel_free(acc);
    }

	
    return 0;
}
