#include <gsl/gsl_sf_erf.h>   // Statistical functions
#include <gsl/gsl_rng.h>      // Random number generator
#include <gsl/gsl_randist.h>  // Random distributions
#include <gsl/gsl_spline.h>   // Spline interpolation
#include <gsl/gsl_vector.h>   // Vector definition
#include <gsl/gsl_min.h>      // Minimization algorithms
#include <gsl/gsl_errno.h>    // Error hanglin
#include <gsl/gsl_math.h>     // Math funcitons and constants.

// Definición del tipo, para pasar parametros a las función 
// a minimizar. Los parametros contatn del spline y del acelerador
// correspondiente

struct minimization_params 
{
    gsl_spline *  s;      // Spline 
    gsl_interp_accel *a;  // Accelerator
};

// La función a maximizar. La lista de parametros p es un estructura
// minmizaton_params.

double function(double x, void * p)
{
    minimization_params * params = (minimization_params *)p;

    gsl_interp_accel  *a = (params->a);
    gsl_spline  *s = (params->s);

// El valor que devuelve la funcion a maximizar es de signo contrario
// al ajuste porque los algoritmos son de minimización. Al cambiar
// de signo, maximisan gsl_spline_eval().
    return -1.0*gsl_spline_eval(s,x,a);
}

// CSPLINEINTENSITY es el porcentaje del valor maximo de la intensidad
// que se usará para acotar el rango donde se buscara el maximo del pico.
#define CSPLINEINTENSITY 80

// Devuelve el centro del alineamiento ne center.
int  GetAlignmentMaximum(double angle[], double intensity[],
			   int pointnumber,double center);
