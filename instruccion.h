//***********************************************
//PROYECTO: DIFRACTOMETRO   N258
//FICHERO:	INSTRUCCION.H

//***********************************************

#ifndef __INSTRUCCION_H
#define __INSTRUCCION_H

#ifndef _WIN32
// _MAX_PATH esta definido en stdlib.h de Windows.
#define _MAX_PATH 260
typedef unsigned int UINT;
#endif

struct tag_instruccion
{
    UINT Handle  __attribute__  ((aligned(1)));
    int	Codigo __attribute__  ((aligned(1)));
    double Param[10]  __attribute__  ((aligned(1)));
    char Comando[_MAX_PATH] __attribute__  ((aligned(1))) ;
    char Instruc[_MAX_PATH] __attribute__  ((aligned(1))) ;
};

typedef struct tag_instruccion instruccion;


#endif





